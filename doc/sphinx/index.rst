.. debye-spectral-density-generator documentation master file,
created by sphinx-quickstart on Sat Jan 23 18:10:59 2016.
.. You can adapt this file completely to your liking,
but it should at least contain the root `toctree` directive.

Debye Spectral Density Generator (DeSpeGo)
==========================================

Contents:

.. toctree::
:maxdepth: 4

        DebyeSpectralGenerator
        IntegrationAlgorithm
        DebyeSpectralGeneratorTest
        DebyeSpectralGeneratorEnsemble
        setup


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

