================================
Debye Spectral Density Generator
================================

:author: Inon Sharony
:e-mail: InonShar@TAU.ac.IL
:date: 2016-01-22
:description: Simulate bath force with Debye power spectrum,
                        approximating using arbitrary finite number of explicit bath modes (implementing Implementation of DOI: 10.1063/1.436895)
                        [#restructuredtextCheatSheet]_.

Notes from 2016-03-18 meeting
=============================
Treat in reverse order:

#. Check correct use of Autocorrelation and FFT functions. Verify Autocorrelation FFT vs. analytically verifiable result:
    :math:`\mathcal F_t [(1-t^2) \exp(-t^2 / 2)](\omega) = \omega^2 \exp(-\omega^2 / 2)`
#. Dampen force autocorrelation when :math:`\omega_D t \gg 1` (e.g. :math:`\omega_D t > 10`).
#. Ensemble average force autocorrelation.
#. Reverse FFT **eqn. I.4** (for general *n*) and compare with force autocorrelation.
#. Plot figure 3: :math:`\omega^{-2} FFT_t[C_{F_f F_f}(t)](\omega)` vs. :math:`\omega/\omega_D`.
#. Is :math:`FFT_t[F_f(t)](\omega) \sim \cos(10 \omega_D t)` where it should be :math:`\cos(\omega_D t)`?
#. From **eqn. III.18a** for :math:`n=4`, we derived :math:`B_0^1 = 2b_0`, instead of assuming :math:`B_0^0 = 1`. No need to try :math:`B_0^0 = 0`, or failing that, to re-derive the entire paper.
#. Solve **eqn. III.18a** for :math:`n=2,3,5` and validate **eqn. III.21**.

.. [#restructuredtextCheatSheet] `reStructuredText cheat sheet`__.
__ http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt