from configparser import ConfigParser
from logging import info, exception, debug, warning
from multiprocessing.pool import Pool, ApplyResult
from os import getpid, getenv, walk
from os.path import join, split
from sys import argv, path
from warnings import warn

from matplotlib import use

from DebyeSpectralGenerator import config_fetch_and_update, current_iso8601_datetime_str
from test.DebyeSpectralGeneratorTest import DebyeSpectralGeneratorTest, backend_name, inspect_caller_name

use(backend_name)

from matplotlib.pyplot import savefig, errorbar, subplot, title, xlabel, ylabel, scatter, fill_between, tight_layout
from numpy import unique, array, sqrt

"""
Ensemble-averages runs of DebyeSpectralGeneratorTest.

.. todo::
    #. Instead of building the CUT on each sub-process,
    could we create a :module:`functools.partial` and pass that to each worker?
    .. seealso:: http://mrcoles.com/blog/3-decorator-examples-and-awesome-python/

    #. Adding module to PYTHONPATH via package/init.py
    .. seealso::
        #. https://docs.python.org/3.5/using/cmdline.html#envvar-PYTHONPATH
        #. https://pymotw.com/3/sys/imports.html
        #. http://stackoverflow.com/a/7259947
"""
__docformat__ = 'reStructuredText'


def work_fork(r):
    """
    Runs on a separate process.

    #. .. warning:: Accepts only top-level code or primitives, from an accessible module
    (under Micro$oft Windoze, that means one with an "if __name__ == '__main__':" statement).

        #. Non-primitive data structures are serialized using pickle, by default.
        .. todo:: instantiate ForkingPickler for DebyeSpectralGeneratorTest.

        #. Alternatively, replace from :class:`multiprocessing.__pool.Pool` by
        :class:`pathos.multiprocessing.ProcessingPool` (uses dill instead of pickle).
        .. seealso:: http://stackoverflow.com/a/21345423/3368225.

    #. Use Pipe/Queue for on-demand inter-process communication or data transfer.
    """
    info("do test and __autocorrelation #%d (pid = %d)" % (r, getpid()))
    cut = set_up_cut()
    time_grid = cut.grid_time()
    cut.do_test()
    autocorrelation, popt, shifted_time_grid, autocorrelation_filename = cut.autocorrelate_fluctuation_force(
        time_grid)
    return autocorrelation_filename


def set_up_cut():
    DebyeSpectralGeneratorTest.setUpClass()
    cut = DebyeSpectralGeneratorTest()
    cut.setUp()
    return cut


class Mapper:
    def __init__(self, num_process_workers, num_realizations, use_async_callbacks=False):
        self._num_process_workers = num_process_workers
        self._num_realizations = num_realizations
        self._use_async_callbacks = use_async_callbacks
        self.__pool = None
        self.__results = []

    def _main_process_callback(self, result):
        info("Processes ran: {} (pid, autocorrelation_filename)".format(result))
        self.__results.append(result)

    @staticmethod
    def _main_process_error_callback(ex):
        exception(ex)

    def _pool_execute(self):
        if self._use_async_callbacks:
            self._pool_apply_async()
        else:
            self.__pool_imap_unordered()

    def __call__(self):
        info("testing %s" % inspect_caller_name(True))

        with Pool(self._num_process_workers) as self.__pool:
            self._pool_execute()

            info("closing the __pool...")
            self.__pool.close()
            info("the __pool is now closed. Joining the __pool...")
            self.__pool.join()
            info("the __pool has joined. Apply __results: {}".format(self.__results))

        assert len(self.__results) == self._num_realizations, "len(__results) == %d != %d == _num_realizations" % (
            len(self.__results), self._num_realizations)
        # caveat: numpy.unique flattens!
        assert len(unique(self.__results)) == self._num_process_workers

    def __pool_imap_unordered(self):
        self.__results.extend(
            [_ for _ in
             self.__pool.imap_unordered(work_fork, range(self._num_realizations),
                                        self._num_realizations / self._num_process_workers)])

    def _pool_apply_async(self):
        async_results = []
        for r in range(self._num_realizations):
            async_results.append(self.__get_worker_from_pool_and_apply_non_blocking(r))
        info("async applied. Async __results: {}".format(async_results))
        for r in async_results:
            debug("waiting for worker {}".format(r))
            r.wait()

    def __get_worker_from_pool_and_apply_non_blocking(self, r) -> ApplyResult:
        args = (r,)
        return self.__pool.apply_async(work_fork, args=args, callback=self._main_process_callback,
                                       error_callback=self._main_process_error_callback)


class Reducer:
    def __init__(self):
        self.__autocorrelation = []
        self.__autocorrelation_sqr = []
        self.__first_two_moments = []

    def _calc_first_two_moments(self):
        job_id = getenv('SGE_O_JOBID', '0')
        out = join("out", "fluctuation_force_autocorrelation")
        n = 0
        for dirpath, dnames, fnames in walk(out):
            for f in fnames:
                if f.find("fluctuation_force_autocorrelation_" + job_id) == -1:
                    continue
                n += 1
                fully_specified_filename = join(dirpath, f)
                debug("averaging in %s (#%d)" % (fully_specified_filename, n))
                self.__aggregate(fully_specified_filename)
        self.__first_two_moments = self._normalize(self.__autocorrelation, n), self._normalize(
            self.__autocorrelation_sqr, n)

    def __aggregate(self, file):
        with open(file) as f:

            is_first = False
            if len(self.__autocorrelation) == 0:
                is_first = True

            i = 0
            for line in f:
                new_entry = [float(x) for x in line.split()]
                if is_first:
                    self.__autocorrelation.append(new_entry)
                    self.__autocorrelation_sqr.append([new_entry[0], new_entry[1] ** 2])
                else:
                    if self.__autocorrelation[i][0] != new_entry[0]:
                        raise RuntimeError("__autocorrelation time value mismatch: existing = %f != %f = new" % (
                            self.__autocorrelation[i][0], new_entry[0]))
                    self.__autocorrelation[i][1] += new_entry[1]
                    self.__autocorrelation_sqr[i][1] += new_entry[1] ** 2
                i += 1

        return i

    @staticmethod
    def _normalize(autocorrelation, n):
        return array(autocorrelation) / n

    def __call__(self):
        self._calc_first_two_moments()
        return self.__first_two_moments


class EnsembleAverager:
    def __init__(self, num_procs):
        self._num_procs = num_procs
        resource_path = DebyeSpectralGeneratorTest.get_resource_path()
        self._realizations_chunksize = 1
        self._use_callbacks = False
        self.configure_test(ConfigParser(), resource_path)
        self.__first_two_moments = None

    def configure_test(self, config, config_path, config_filename="test_configuration.ini"):
        fully_specified_config = join(config_path, config_filename)
        config.read(fully_specified_config)
        self._realizations_chunksize = config_fetch_and_update(config, "simulation", "_realizations_chunksize", int,
                                                               fallback=1)
        self._use_callbacks = config_fetch_and_update(config, "simulation", "_use_async_callbacks", bool,
                                                      fallback=False)

    def _map(self):
        DebyeSpectralGeneratorTest.setUpClass()
        executor = Mapper(self._num_procs, self._num_procs * self._realizations_chunksize, self._use_callbacks)
        executor()

    def __get_moment_component(self, moment, component):
        """
        :arg moment: One-based. i.e. :math:`1 \le m \le 2`.
        :type moment: int
        :arg component: Time (0) or moment (1).
        :type component: int
        """
        return self.__first_two_moments[moment - 1][:, component]

    def __plot_ensemble_averaged_autocorrelation(self, subplot_index=None):
        subplot_title = 'Ensemble-Averaged Bath Fluctuation-Force Autocorrelation'
        debug("plot %s" % subplot_title)

        if subplot_index is not None:
            subplot(subplot_index)
        title(subplot_title)
        xlabel(r'$t$ / Inverse of Debye frequency $\left(\omega_D^{-1}\right)$')
        ylabel(r'$C_{F_f F_f}(t) / {F^{*}}^2$')

        x = self.__get_moment_component(1, 0)
        y = self.__get_moment_component(1, 1)
        y2 = self.__get_moment_component(2, 1)
        yerr = sqrt(y2 - y ** 2)
        if len(x) < 100:
            errorbar(x, y, yerr,
                     ecolor=(0, 0, 1, 0.1), elinewidth=1, capsize=1, capthick=1, marker='o', mfc='k', ms=1)
        else:
            scatter(x, y, s=1, c='k')
            fill_between(x, y + yerr, y - yerr, color='b', alpha=0.2)

    def _reduce(self):
        averager = Reducer()
        self.__first_two_moments = averager()
        self.__plot_ensemble_averaged_autocorrelation(211)

    def _post_process(self):
        tester = set_up_cut()
        average_autocorrelation = self.__get_moment_component(1, 1)
        power_spectra_args = tester.power_spectra(average_autocorrelation)
        tester.plot_bath_power_spectrum(*power_spectra_args, 212)

    @staticmethod
    def __get_relative_file_name_no_ext():
        path_tail = split(__file__)[1]
        ext_pos = path_tail.rfind('.')
        return path_tail[:ext_pos]

    def _save_fig(self):
        tight_layout()
        name = self.__get_relative_file_name_no_ext()
        plot_file_name = name.replace(" ", "-") + '_' + current_iso8601_datetime_str() + ".png"
        savefig('out/plot/' + plot_file_name, dpi=96 * 3)

    def __call__(self):
        self._map()
        self._reduce()
        self._post_process()
        self._save_fig()


def main(num_procs):
    ensembler = EnsembleAverager(num_procs)
    ensembler()


if __name__ == '__main__':
    debug(path)
    assert len(argv) > 1, "missing command-line argument/s"
    if len(argv) > 2:
        warn(UserWarning("extraneous command-line argument/s ignored!"))
        warning("extraneous command-line argument/s ignored!")
    main(int(argv[1]))
    info("normal termination")
