import cProfile
import inspect
import unittest
from cmath import polar
from collections import namedtuple
from configparser import ConfigParser
from itertools import cycle
from logging import debug, info, exception, error
from math import inf
from multiprocessing.pool import Pool
from os import getcwd, getenv, getpid
from os.path import isdir, exists
from pprint import pformat
from time import perf_counter
from typing import Callable, Tuple, Iterable
from warnings import warn

import nose
import numba
import numpy
import scipy.optimize
from matplotlib import use, is_interactive, get_backend
from nose.loader import TestLoader
from nose.tools import ok_, nottest, istest
from numba.decorators import jit
from numpy import empty, correlate, unique, finfo, sqrt, linspace, isfinite
from numpy.fft import rfft, rfftfreq
from numpy.ma import absolute, argmax, diag, array, zeros

from despego.DebyeSpectralGenerator import DebyeSpectralGenerator, setup_logger, current_iso8601_datetime_str, \
    configure_generator, config_fetch_and_update

IS_NON_INTERACTIVE = True
if IS_NON_INTERACTIVE:
    backend_name = 'agg'  # => png ( / 'svg' / 'cairo' / etc.)
    use(backend_name)  # http://stackoverflow.com/a/3054314
    from matplotlib.pyplot import figure, plot, savefig, title, xlabel, ylabel, rc, subplot, ioff, ion, tight_layout, \
        semilogy, scatter, gca
else:
    from matplotlib.pyplot import figure, plot, savefig, title, xlabel, ylabel, rc, subplot, ioff, ion, tight_layout


def inspect_caller_name(is_current_frame=False):
    frames_up = 1
    if not is_current_frame:
        frames_up += 1
    caller_frame_info = None
    try:
        stack = inspect.stack()
        caller_frame_info = stack[frames_up]
        return caller_frame_info.function
    except Exception as e:
        exception(str(e))
        if caller_frame_info is None:
            return
        return caller_frame_info[frames_up + 1]


@jit
def find_cutoff(input_list, threshold=0.01):
    n = len(input_list)
    max_held = 0
    for i in range(n):
        x = polar(input_list[i])[0]
        max_held = max(max_held, x)
        if max_held > 0 and x / max_held < threshold:
            return i
    return n


@jit
def exponential_divergence(t, c0, tau_m):
    return exponentially_decaying_cosine(t, c0, -tau_m, 0)


@jit
def exponentially_decaying_cosine(t, c0, tau_m, omega):
    return c0 * numpy.exp(-t / tau_m) * numpy.cos(omega * t)


class ValueAndUncertainty(namedtuple('FittedParameter', ['value', 'uncertainty'])):
    """
    Save memory by overriding the initialization of the object's dictionary with just the underlying tuple.
    """

    __slots__ = ()

    def __str__(self):
        return "{} +/- {}".format(self.value, self.uncertainty)


class DebyeSpectralGeneratorTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        """
        cls variables (cf. static fields) initialization

        :var cls.sample_rate: No need for :math:`\ni \sim \omega_D^{-1}dt^{-1}` much less than unity,
            since we're not interested in :math:`\omega \gg \omega_D`.

        :var cls.n_slow_periods: No need for :math:`t_f \sim n \omega_D^{-1}` much more than :math:`\omega_D^{-1}`,
            since SNR drops
        """

        setup_logger()

        TestLoader.sortTestMethodsUsing = None

        ioff() if IS_NON_INTERACTIVE else ion()
        debug("is matplotlib interactive? {}, backend: {}".format(is_interactive(), get_backend()))

        cls.Range = namedtuple("Range", ['min', 'max'])

        info(numba.__version__)

        resource_path = cls.get_resource_path()
        cls.configure_class(resource_path)

    @classmethod
    def get_resource_path(cls):
        if isdir('resources'):
            resource_path = 'resources'
        elif isdir('../resources'):
            resource_path = '../resources'
        else:
            raise Exception("can't locate resources directory!")
        return resource_path

    @classmethod
    def configure_class(cls, resource_path, config_filename="test_configuration.ini"):
        config = ConfigParser()
        cls.mass, cls.omega_d, cls.temperature, cls.n, cls.is_use_recursive_equations, cls.integrator, \
        cls.sample_rate, cls.dt = configure_generator(config, resource_path, config_filename)
        cls.n_slow_periods, cls.profiler_n_slow_periods, cls.num_realizations, cls.num_process_workers = \
            cls.configure_test(config, resource_path, config_filename)

    def setUp(self):
        """
        In lieu of __init__, which is not trivially overridden
        """
        particle_omega = 10 * self.omega_d
        self.omega2 = particle_omega * particle_omega

        self.omega_range = self.Range(min=min(self.omega_d, particle_omega), max=max(self.omega_d, particle_omega))
        self.sut = DebyeSpectralGenerator(self.mass, self.omega_d, self.temperature, self.n,
                                          self.is_use_recursive_equations, self.integrator,
                                          sample_rate=self.sample_rate)
        self.dt = self.sut.dt
        assert 0 < self.dt < 5e-2 / particle_omega, \
            "dt (%f) is either non-positive or not less than 0.05 / particle omega (%f)" % (self.dt, particle_omega)
        self.init_time_steps_by_n_slow_periods(self.n_slow_periods)

        cls_dict = self.__dict__.copy()  # deepcopy(self.__dict__)
        cls_dict.pop('_F_f')
        cls_dict.pop('xt')
        info(pformat(cls_dict))

    @classmethod
    def tearDownClass(cls):
        info("tearing down %s" % cls.__name__)

    def init_time_steps_by_n_slow_periods(self, n_slow_periods=10):
        """
        :param n_slow_periods:
        :type n_slow_periods: float
        """
        self.t_f = n_slow_periods / self.omega_range.min
        self.n_steps = int(self.t_f / self.dt)
        # self.n_segments = min(self.n_steps, 1000)
        self.n_segments = self.n_steps
        self.segment_length = int(self.n_steps / self.n_segments)
        self.n_steps = self.n_segments * self.segment_length
        self._F_f = zeros(self.n_segments)
        self.xt = empty([self.n_steps, 2])

    @istest
    def test_test(self):
        info("testing %s" % inspect.currentframe().f_code.co_name)
        self.n = 0
        self.do_test()

    @nottest
    def do_test(self):
        try:
            self._construct_generator_now()
            self._velocity_verlet()
            self.log_xt()
        except Exception as e:
            exception(e)
            raise e

    @jit
    def log_xt(self):

        # for tau in range(self.n_steps):
        #     debug("{}".format(self.xt[tau]))

        debug("last phase configuration = {}".format(self.xt[-1]))
        debug("last fluctuation force = {}".format(self._F_f[-1]))

        for tau in range(self.xt.shape[0]):
            seg = tau / self.segment_length if tau % self.segment_length == 0 else -1
            is_valid = isfinite(self._F_f[seg]) if seg >= 0 else True
            for i in range(2):
                is_valid &= isfinite(self.xt[tau][i])
            if not is_valid:
                raise RuntimeError(
                    "phase configuration or fluctuation force contain at least one invalid number: inf or NaN")

    @jit
    def theoretical_mode_density(self, magnitude, omega):
        return magnitude * omega * omega / (1 + pow(omega / self.omega_d, 2 * self.n))

    def plot_autocorrelation(self):
        rc('text', usetex=True)

        figure(figsize=(12, 9), dpi=96)

        time_grid = self.grid_time()

        subplot_title = 'Trajectory'
        debug("plot %s" % subplot_title)
        subplot(321)
        ts = linspace(0, self.t_f, num=self.n_segments)
        xs = self.xt[:, 1][::self.segment_length]
        scatter(ts, xs, s=1)
        # plot(ts, xs, markevery=self.segment_length)
        title(subplot_title)
        xlabel(r't / inverse Debye frequency $\left(\omega_D^{-1}\right)$')
        ylabel('Position')
        # 'Phase-space configuration'

        subplot_title = 'Fluctuation force'
        debug("plot %s" % subplot_title)
        subplot(322)
        plot(time_grid, zeros(self.n_segments), 'k', time_grid,
             self._F_f, 'b:')
        title(subplot_title)
        xlabel(r't / inverse Debye frequency $\left(\omega_D^{-1}\right)$')
        ylabel('Force')

        epsilon = finfo(type(self._F_f[0])).eps
        if any((absolute(f) > epsilon) for f in self._F_f):
            subplot_title = 'Fluctuation force magnitude'
            debug("plot %s" % subplot_title)
            subplot(323)
            try:
                popt, pcov = curve_fit(exponential_divergence, time_grid, absolute(self._F_f))
                if abs(popt[0]) > 1e-13:
                    semilogy(time_grid, exponential_divergence(time_grid, *popt), 'k', time_grid, absolute(self._F_f),
                             'b:')
                else:
                    semilogy(time_grid, absolute(self._F_f), 'b-')
                title(subplot_title)
                xlabel(r't / inverse Debye frequency $\left(\omega_D^{-1}\right)$')
                ylabel('Force magnitude')
            except ValueError as ve:
                error("fluctuation force = {},... ; absolute(_) = {},...".format(self._F_f[:10],
                                                                                 absolute(self._F_f[:10])))
                exception(str(ve))
        else:
            warn("not all fluctuation force segments are positive definite!", RuntimeWarning)

        subplot_title = 'Autocorrelation of the force exerted on the particle by the bath'
        debug("plot %s" % subplot_title)
        subplot(324)

        autocorrelation, popt, shifted_time_grid, autocorrelation_filename = self.autocorrelate_fluctuation_force(
            time_grid)

        plot_args = []
        if abs(popt[0]) >= 1e-13:
            plot_args += [shifted_time_grid, exponentially_decaying_cosine(absolute(shifted_time_grid), *popt), 'k']
        plot_args += [shifted_time_grid, autocorrelation, 'b-']
        plot(*plot_args)
        title(subplot_title)
        xlabel(r't / inverse Debye frequency $\left(\omega_D^{-1}\right)$')
        ylabel(r'Force __autocorrelation $\left<F_f(0)F_f(t)\right>$')

        power_spectra_args = self.power_spectra(autocorrelation)
        self.plot_bath_power_spectrum(*power_spectra_args, 325)

        tight_layout()

        debug("current working directory = %s" % getcwd())
        plot_file_name = inspect_caller_name().replace(" ", "-") + '_' + current_iso8601_datetime_str() + ".png"
        file_dpi = 96 * 3
        try:
            savefig('../out/plot/' + plot_file_name, dpi=file_dpi)
        except FileNotFoundError as fnfe:
            warn(ResourceWarning(fnfe))
            savefig('out/plot/' + plot_file_name, dpi=file_dpi)

        return autocorrelation_filename

    @staticmethod
    def plot_bath_power_spectrum(positive_frequency_grid, power_spectrum, theoretical_mode_density_values,
                                 cutoff, subplot_index=None):
        subplot_title = 'Comparison of simulated bath force power spectrum with theoretical mode density'
        debug("plot %s" % subplot_title)

        if subplot_index is not None:
            subplot(subplot_index)
        title(subplot_title)
        xlabel(r'frequency $\left(\omega\right)$ / Debye frequency $\left(\omega_D\right)$')
        ylabel(r'energy density in inverse Debye frequency $\left(\omega_D^{-1}\right)$')

        cutoff_positive_frequency_grid = positive_frequency_grid[:cutoff]
        cutoff_real_power_spectrum = power_spectrum.real[:cutoff]
        cutoff_imag_power_spectrum = power_spectrum.imag[:cutoff]
        cutoff_theoretical_mode_density = array(theoretical_mode_density_values[:cutoff])

        shapes = [cutoff_positive_frequency_grid.shape, cutoff_real_power_spectrum.shape,
                  cutoff_theoretical_mode_density.shape]
        debug("power spectrum data structure shapes = %s" % pformat(shapes))

        ax = gca()
        series = [cutoff_real_power_spectrum, cutoff_imag_power_spectrum, cutoff_theoretical_mode_density]
        label_cycle = cycle(["Real", "Imag", "Theory"])
        color_cycle = cycle(["b", "r", "k"])
        size_cycle = cycle([2, 1, 1])
        for y in series:
            ax.scatter(cutoff_positive_frequency_grid, y, label=next(label_cycle), color=next(color_cycle),
                       s=next(size_cycle))

    def power_spectra(self, autocorrelation):
        power_spectrum = rfft(autocorrelation)
        positive_frequency_grid = rfftfreq(self.n_segments, self.dt * self.segment_length)
        # power_spectrum = rfft([c for n, c in enumerate(autocorrelation) if n >= len(autocorrelation) / 2])
        # positive_frequency_grid = rfftfreq(int(self.n_segments / 2) + 1, self.dt * self.segment_length)
        debug("frequency grid = start={}:step={}:stop={}".format(positive_frequency_grid[0],
                                                                 positive_frequency_grid[1] - positive_frequency_grid[
                                                                     0], positive_frequency_grid[-1]))
        assert len(power_spectrum) == len(positive_frequency_grid)

        def index_of_first_positive_frequency_above_threshold(threshold):
            """
            Nested function with gratuitous closure (positive_frequency_grid). Improves readability??
            :param threshold:
            :return: index of first positive frequency above threshold
            """
            return argmax(positive_frequency_grid > threshold)

        theoretical_mode_density_values = []
        for omega in positive_frequency_grid:
            theoretical_mode_density_values.append(
                self.theoretical_mode_density(numpy.max(absolute(power_spectrum.real)), omega))
        index_of_first_positive_freq_above_threshold = \
            index_of_first_positive_frequency_above_threshold(self.omega_d * 2)
        cutoff = index_of_first_positive_freq_above_threshold if index_of_first_positive_freq_above_threshold > 1 \
            else -1  # max(find_cutoff(power_spectrum), find_cutoff(theoretical_mode_density_values))
        cutoff_power_spectrum = find_cutoff(power_spectrum)
        cutoff_theoretical_mode_density = find_cutoff(theoretical_mode_density_values)
        debug(
            "power_spectrum_cutoff = {}, theoretical_mode_density_cutoff = {}, "
            "index_of_first_positive_frequency_above_ 2 omega_D = {} , cutoff = {}".format(
                cutoff_power_spectrum, cutoff_theoretical_mode_density,
                index_of_first_positive_freq_above_threshold, cutoff))
        return positive_frequency_grid, power_spectrum, theoretical_mode_density_values, cutoff

    def autocorrelate_fluctuation_force(self, time_grid):
        autocorrelation = correlate(self._F_f, self._F_f, mode='same')
        popt, pcov = curve_fit(exponentially_decaying_cosine, time_grid, self._F_f)
        shifted_time_grid = array(
            time_grid - self.dt * self.segment_length * self.n_segments / 2)  # TODO is this correct?
        autocorrelation_filename = self.print_autocorrelation(shifted_time_grid, autocorrelation)
        return autocorrelation, popt, shifted_time_grid, autocorrelation_filename

    def grid_time(self):
        time_grid = []
        for seg in range(self.n_segments):
            time_grid.append(self.dt * self.segment_length * seg)
        return array(time_grid)

    @staticmethod
    def print_autocorrelation(shifted_time_grid, autocorrelation):
        trajectory = getenv('SGE_O_JOBID', '0') + '_' + getenv('SGE_O_TASKNUM', str(getpid()))
        path = 'out/' if exists('out/') else '../out/'
        filename = path + "fluctuation_force_autocorrelation/fluctuation_force_autocorrelation_" + trajectory + ".csv"
        with open(filename, mode='wt') as fs:
            for i in range(shifted_time_grid.size):
                fs.write("%g %g\n" % (shifted_time_grid[i], autocorrelation[i]))
        return filename

    @istest
    def test_zero_temp(self):
        info("testing %s" % inspect.currentframe().f_code.co_name)
        self.temperature = 0
        self.do_test()
        abs_tol = 1e-5
        ok_(numpy.all(absolute(self._F_f < abs_tol)),
            msg="max abs fluctuation force = %f >= %f = abs_tol" % (numpy.max(absolute(self._F_f)), abs_tol))
        self.plot_autocorrelation()

    @istest
    def test_positive_temp(self):
        # raise nose.SkipTest()
        info("testing %s" % inspect.currentframe().f_code.co_name)
        self.do_test()
        self.plot_autocorrelation()

    @classmethod
    @nottest
    def work_fork(cls, r):
        info("do test and __autocorrelation #%d (pid = %d)" % (r, getpid()))
        DebyeSpectralGeneratorTest.setUpClass()
        tester = DebyeSpectralGeneratorTest()
        tester.setUp()
        tester.do_test()
        autocorrelation_filename = tester.plot_autocorrelation()
        return autocorrelation_filename

    @nottest
    def main_process_callback(self, result):
        info("Processes ran: {} (pid, autocorrelation_filename)".format(result))
        self.apply_results.append(result)

    @istest
    def test_positive_temp_ensemble(self):
        info("testing %s" % inspect.currentframe().f_code.co_name)

        self.apply_results = []

        async_results = []
        with Pool(self.num_process_workers) as pool:
            for r in range(self.num_realizations):
                args = (r,)
                async_results.append(pool.apply_async(self.work_fork, args=args, callback=self.main_process_callback))
            info("async applied. Async __results: {}".format(async_results))

            for r in async_results:
                debug("waiting for worker {}".format(r))
                r.wait()

            info("closing the __pool...")
            pool.close()
            info("the __pool is closed. Joining the __pool...")
            pool.join()
            info("the __pool has joined. Apply __results: {}".format(self.apply_results))

        assert len(self.apply_results) == self.num_realizations
        assert len(unique(self.apply_results)) == self.num_process_workers

    @classmethod
    @nottest
    def configure_test(cls, config, resource_path, config_filename):
        config.read(resource_path + '/' + config_filename)

        n_slow_periods = config_fetch_and_update(config, "test", "n_slow_periods", float, fallback=0.001)
        profiler_n_slow_periods = config_fetch_and_update(config, "profiler", "n_slow_periods", float, fallback=0.001)
        num_realizations = config_fetch_and_update(config, "ensemble", "_num_realizations", int, fallback=2)
        num_process_workers = config_fetch_and_update(config, "ensemble", "_num_process_workers", int, fallback=2)

        fp = open(resource_path + '/' + config_filename + '.bak', mode='wt')
        config.write(fp)

        return n_slow_periods, profiler_n_slow_periods, num_realizations, num_process_workers

    def _construct_generator_now(self):
        self.sut = DebyeSpectralGenerator(self.mass, self.omega_d, self.temperature, self.n,
                                          self.is_use_recursive_equations, self.integrator, self.sample_rate, self.dt)
        ok_(self.sut)

    @istest
    def test_profile(self):
        info("testing %s" % inspect.currentframe().f_code.co_name)
        try:
            self._construct_generator_now()
            self.init_time_steps_by_n_slow_periods(self.profiler_n_slow_periods)
            self.__log_profile()
        except Exception as e:
            exception(e)
            raise e

    def __log_profile(self):
        debug("needs self if profiling class methods (e.g. self._velocity_verlet()) {}".format(self))
        profile = cProfile.Profile()
        profile.runctx('self._velocity_verlet()', globals(), locals())
        profile.print_stats(sort=1)
        for entry in profile.getstats():
            info(entry)

    @jit
    def _velocity_verlet(self):
        hdt2 = self.dt * self.dt / 2

        x = zeros(3)  # :raw-latex:`\(\frac d^i dt^i x = x, v, a\)`
        x[0] = 0.1 * pow(self.temperature, 2) / (self.mass * self.omega2)

        init_perf_count = perf_counter()

        debug("t, x, v, a")
        for tau in range(self.n_steps):
            x[0] += x[1] * self.dt + x[2] * hdt2  # :raw-latex:`\(x(t) = x(v(t - dt), a(t - dt))\)`
            x[1] += x[2] * self.dt / 2  # First Trotter update :raw-latex: `v(a(t - dt))\)`
            x[2] = self.__accelerate(tau, x[0], x[1])
            x[1] += x[2] * self.dt / 2  # Second Trotter update :raw-latex: `\(v(a(t))\)`
            xt = (tau * self.dt, x[0])
            self.xt[tau] = xt
            if tau % max(int(self.n_steps / 20), 1) == 0:
                ratio_done = max(tau, 1) / self.n_steps
                debug("time step = %d (%d%%) => ~%s left" % (
                    tau, 100 * ratio_done, format_seconds((1 / ratio_done - 1) * (perf_counter() - init_perf_count))))
            if not all(isfinite(self.xt[tau])):
                warn("Phase space configuration is not finite! {}".format(self.xt[tau]), RuntimeWarning)

        info("velocity_verlet simulation terminated (%d time-steps)" % self.n_steps)

    @jit
    def __accelerate(self, tau, x, v):
        """
        :raw-latex: `\(-(1/m) * dV/dx + F_f / m + F_r / m\)`
        :param tau:
        :type tau: int
        :param x:
        :type x: float
        :param v:
        :type v: float
        :return:
        :rtype: float
        """
        bath_forces = self.sut(tau, v)
        self.__update_coarse_grained_fluctuation_force(bath_forces, tau)
        return (self._f(x) + bath_forces.dissipation + bath_forces.fluctuation) / self.mass

    @jit
    def __update_coarse_grained_fluctuation_force(self, bath_forces, tau):
        seg = int(tau / self.segment_length)
        self._F_f[seg] += bath_forces.fluctuation
        if tau % self.segment_length == 0:
            self._F_f[seg] /= self.segment_length

    @jit
    def _f(self, x):
        return -self.mass * self.omega2 * x


@jit
def format_seconds(seconds):
    hours = int(seconds / (60 * 60))
    minutes = int(seconds / 60) - hours * 60
    seconds = seconds - hours * 60 * 60 - minutes * 60
    return "%02d:%02d:%02f" % (hours, minutes, seconds)


def curve_fit(f: Callable[..., float], xdata: Iterable, ydata: Iterable) -> Tuple:
    try:
        max_n_function_evaluations = int(1e4)
        curve_fit_args = [f, xdata, ydata]
        if scipy.__version__ >= '0.17.0':
            curve_fit_kwargs = {'bounds': (0, inf), 'method': 'trf', 'max_nfev': max_n_function_evaluations}
            popt, pcov = scipy.optimize.curve_fit(*curve_fit_args, **curve_fit_kwargs)
        else:
            curve_fit_kwargs = {'maxfev': max_n_function_evaluations}
            popt, pcov = scipy.optimize.curve_fit(*curve_fit_args, **curve_fit_kwargs)
        values_and_uncertainties = [ValueAndUncertainty(*p) for p in zip(popt, sqrt(diag(pcov)))]
        debug("{} curve fit parameters = {}".format(f.__name__, values_and_uncertainties))
        if any(absolute(p.uncertainty / p.value) > 1 for p in values_and_uncertainties):
            raise RuntimeError("relative uncertainty in at least one parameter is greater than unity!")
    except Exception as e:
        # exception(str(e))
        dim_params = len(inspect.signature(f).parameters) - 1
        zeros_tuple = tuple([0] * dim_params)
        popt, pcov = zeros_tuple, zeros_tuple
        values_and_uncertainties = [ValueAndUncertainty(*p) for p in zip(popt, sqrt(diag(pcov)))]
        debug("{} zeroed curve fit parameters = {}".format(f.__name__, values_and_uncertainties))
    return popt, pcov


if __name__ == '__main__':
    nose.main()
