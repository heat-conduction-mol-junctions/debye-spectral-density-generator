"""
https://github.com/numba/numba/issues/1312
"""

import numba
import numpy as np

print(numba.__version__)
# '0.20.0'

@numba.jit(nopython=True)
def f():
    x = np.arange(5)
    return x[1:] - x[:-1]    # something like `x[1:].sum()` works fine though
    
f()

