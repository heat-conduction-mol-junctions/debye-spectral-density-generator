"""
Debye Spectral Density DebyeSpectralGenerator
(implementation of DOI: 10.1063/1.436895)

:author: Inon Sharony
:contact: InonShar@TAU.ac.IL
:organization: Tel Aviv University Department of Chemical Physics
:since:  Jan, 2016


Input:
    mass(float): particle mass

    omega_d(float): Debye frequency of the bath
    temperature(float): bath temperature, in units of the Debye frequency times Planck's constant divided by
    Boltzmann's constant

    n(int): number of 'modes'

    t_f(float): simulated time, in multiples of the inverse of the Debye frequency

    dt(float): (optional) time step, in multiples of the inverse of the Debye frequency. If not supplied,
    it is calculated by calculate_dt.

Calculation of time-invariant quantities (static coefficients):
        sigma(int): 1/ (2 sin( pi/2 * 3/n ) )
        a(p: int): int =
        b(p: int): int =
        A(p: int): int[2,2] =

Generating doc:

    Sphinx:
        cd $PROJECT_ROOT
        SPHINX_DIR="doc/sphinx"
        mkdir -p $SPHINX_DIR
        cd $SPHINX_DIR
        sphinx-apidoc -o . -e -P -M -F -A "Inon Sharony" -V "0.1" $PROJECT_ROOT
        make html
        firefox _build/html/index.html

Implementation notes:

    Numba:
        Numba does has no opcode to handle exceptions or comprehensions... (!)
        There is, however, support for raising exceptions (provided that they're handled in a non-jitted function)
        http://numba.pydata.org/numba-doc/dev/reference/pysupported.html#constructs

Idiomatic/Pythonic refactorings:

    1. For all functions (not called in the middle of a tight loop), change any positional arguments (esp. of types bool or int) into keyword arguments, for clarity.
    2. For all functions returning a tuple of values, change to a named tuple for clarity (they're self-documenting!).
    3. Replace appending to lists in loops with list comprehensions :code:`[x for x in xs]`, and list comprehensions with generator expressions :code:`(x for x in xs)`.
    4. Replace setters & getters with @properties
    5. For simultaneous multiple-variable-state updates, use tuple unpacking:
        .. code::
            def permute_cyclically(a,b,c):
                a, b, c = c, a, b
    6. Replace try-finally with 'with' (context managers)
    7. Separate business-logic from administrative-logic by wrapping the business-logic with an administrative-logic @decorator.
        .. code::

            @remember_to_also_do_admin(with_some_arg)
            def business():
                # conduct some particular business here

            def admin_before_business(*args, **kwargs):
                # administrative-logic to execute before business-logic

            def admin_after_business(*args, **kwargs):
                # administrative-logic to execute after business-logic

            def remember_to_also_do_admin(net_business, *warpper_args, **wrapper_kwargs):

                @wraps(wrapped_business_function)
                def wrapped(*args, **kwargs):
                    admin_before_business(*args, **kwargs)
                    result = net_business(*args, **kwargs)
                    admin_after_business(*args, **kwargs)
                    return result

                return wrapped

    Thanks, Raymond Hettinger!
"""

import cmath
import math
from argparse import ArgumentParser
from collections import namedtuple
from configparser import ConfigParser
from datetime import datetime
from logging import basicConfig, critical, info, debug, DEBUG, getLogger, StreamHandler, Formatter, exception
from math import pow, pi, exp, sqrt
from random import gauss
from warnings import warn

from numba.decorators import jit
from numpy import zeros, array, ones

from despego.IntegrationAlgorithm import IntegrationAlgorithm


def kelvins_to_wave_numbers(kelvins):
    return kelvins * 0.695


def angular_frequency_to_period(omega):
    return 1 / (2 * math.pi * omega)


def picoseconds_to_inverse_wave_numbers(picoseconds):
    return picoseconds / 33.35641


class StaticCoefficients:
    @jit
    def __init__(self, n):
        self.n = n

        self.hn = int(n / 2)
        if self.hn <= 0:
            critical("number of 'modes' = %d < 2 = minimum!" % self.hn)
            self.a = None
            self.A = None
            self.sigma = None
            return

        hpi = pi / 2  # half pi
        self.a = zeros([2, n])
        self.A = zeros([n, 2, 2])
        for p in range(n):
            phi = hpi * (2 * p + 1) / n  # angle to n uniform-distributed points on circumference of upper semi-circle
            e = cmath.rect(1, phi)
            self.a[0, p] = e.real
            self.a[1, p] = e.imag
            self.A[p] = array([[-self.a[0, p], self.a[1, p]], [-self.a[1, p], -self.a[0, p]]])

        self.sigma = 1 / (2 * self.a[0, 0])


class DissipativeForce:
    def __init__(self, mass, omega_d, static_coefficients, dt):
        """
        :type self: DissipativeForce
        :param mass: particle mass in statics.m.u.
        :type mass: int
        :param omega_d: Debye frequency, in wave-numbers
        :type omega_d: float
        :param static_coefficients: static statics of the 'modes' (i.e. a_p and b_p)
        :type static_coefficients: StaticCoefficients
        :param dt: time step duration, in inverse Debye frequency
        :type dt: float
        :return: dissipative force at the given time-step
        :rtype: float
        """
        info("initializing DissipationForce object with mass = {}, omega_d = {}, static_coefficients = {}, "
             "and dt = {}".format(mass, omega_d, static_coefficients, dt))
        self.statics = static_coefficients

        self.mass_omega_d = mass * omega_d

        self.dt = dt

        self.S_e = 0
        self.dSdt_e = 0
        self.S = zeros([2, self.statics.hn])
        self.dSdt = zeros([2, self.statics.hn])
        # debug("S = {}, dS/dt = {}".format(self.S, self.dSdt))

    @jit
    def __propagate_init_step(self, v_prev):
        """
        Initialize the 'mode' coordinates and velocities for the current time-step
        :type self: DissipativeForce
        :param v_prev: particle velocity at the previous time-step
        :type v_prev: float
        """
        # debug("initializing propagation accumulators")
        self.dSdt_e = -self.S_e + v_prev
        self.S_e += self.dSdt_e * self.dt

    @jit
    def __propagate_for_p(self, p, v_prev):
        """
        For statics given 'mode' order, propagate the 'modes'
        :type self: DissipativeForce
        :param p: 'mode' order
        :type p: int
        """

        # debug("statics.A.shape = {}, S.shape = {}, dSdt.shape = {}".format(self.statics.A.shape, self.S.shape,
        #                                                                   self.dSdt.shape))

        # self.dSdt[:, p] += dot(self.statics.A[p, :, :], self.S[:, p]).transpose()
        s_shape = self.S.shape
        for i in range(2):
            self.dSdt[i, p] = v_prev if i == 1 else 0
            for j in range(2):
                # try:
                self.dSdt.reshape(s_shape)[i, p] += self.statics.A[p, i, j] * self.S[j, p]
                # except:
                #     error("i = %d, j = %d, p = %d" % (i, j, p))
                #     error("{} = {} . {}".format(self.dSdt.reshape(s_shape).shape, self.statics.A.shape, s_shape))
                #     error("{} = {} . {}".format(self.dSdt.reshape(s_shape), self.statics.A, self.S))
                #     raise
            # try:
            self.S[i, p] += self.dSdt.reshape(s_shape)[i, p] * self.dt
            # except:
            #     error("i = %d, p = %d" % (i, p))
            #     error("{} += {} * dt".format(s_shape, self.dSdt.reshape(s_shape).shape))
            #     error("{} += {} * dt".format(self.S, self.dSdt.reshape(s_shape)))
            #
            #     error("%f" % self.S[i, p])
            #     error("%f" % self.dSdt.reshape(s_shape)[i, p])
            #
            #     raise

    @jit
    def __s(self, p, ap, bp):
        """
        :type self: DissipativeForce
        :param p: 'mode' order
        :type p: int
        :param ap: Real part (cosine)
        :type ap: int
        :param bp: Imaginary part (sine)
        :type bp: int
        :return: contribution of given 'mode' coordinate and order
        :rtype: int
        """
        return 2 * ap * bp * self.S[1, p] + (bp * bp - ap * ap) * self.S[0, p]

    @jit
    def __zeta(self, p, tau):
        """
        :type self: DissipativeForce
        :param p: 'mode' order
        :type p: int
        :param tau: reduced (unit-less) time
        :type tau: float
        :return: contribution to the dissipative force from statics given order of 'mode'
        :rtype: float
        """
        ap = self.statics.a[0, p]
        bp = self.statics.a[1, p]
        return exp(-bp * tau) * self.__s(p, ap, bp)

    @jit
    def __call__(self, tau, v_prev):
        """
        :type self: DissipativeForce
        :param tau: reduced (unit-less) time
        :type tau: float
        :param v_prev: particle velocity at the previous time-step
        :type v_prev: float
        :return: Dissipative force at statics given time-step
        :rtype: float
        """

        if self.statics.hn == 0:
            return 0

        self.__propagate_init_step(v_prev)

        f_d = 0 if self.statics.n % 2 == 0 else self.S_e / 2
        for p in range(self.statics.hn):
            if False:
                debug("S = {}, dS/dt = {}".format(self.S, self.dSdt))
            self.__propagate_for_p(p, v_prev)
            f_d += self.__zeta(p, tau)
        f_d /= -1 * self.statics.sigma * self.mass_omega_d
        if not math.isfinite(f_d):
            warn("Calculated dissipative force is not finite! {}".format(f_d), RuntimeWarning)
        return f_d


class FluctuationForce:
    def __init__(self, integrator, omega_d, static_coefficients, dt, random_force_autocorrelation):
        """
        Calculates fluctuation force one time-step at a time
        :param integrator:
        :type integrator: IntegrationAlgorithm
        :param static_coefficients:
        :type static_coefficients: StaticCoefficients
        :param dt:
        :type dt: float
        :return:
        :rtype: FluctuationForce
        """
        info("initializing FluctuationForce object with {} integrator, static_coefficients = {}, and dt = {}".format(
            integrator, static_coefficients, dt))
        self.integrator = IntegrationAlgorithm[integrator]

        self.omega_d = omega_d

        self.sqrt_autocorrelation = sqrt(random_force_autocorrelation)

        self.statics = static_coefficients

        self.r = 0

        self.dt = dt

    @jit
    def _cn(self):
        """
        :return: ((4 ** (n-1)) / sigma) * prod{{(b((p-1) / 2) ** 2)}_1}^{n-1}
        :rtype: int
        """
        pre_factor = pow(4, self.statics.n - 1) / self.statics.sigma
        product = 1
        for p in range(1, self.statics.n):
            product *= self.statics.a[1, int((p - 1) / 2)]
        return pre_factor * product

    @jit
    def _generate_r(self):
        """
        :return: Stochastic term in fluctuation force
        :rtype: float
        """
        z = gauss(0, self.sqrt_autocorrelation)
        n_ = pow(self.omega_d, self.statics.n + 1)
        return z * n_

    @jit
    def __call__(self):
        """
        :return: Random force for the current time-step
        :rtype: float
        """

        if self.statics.hn == 0 or abs(self.sqrt_autocorrelation) < 1e-5:
            return 0

        r = self._generate_r()
        f_r = self._propagate(r)
        if not math.isfinite(f_r):
            warn("Calculated fluctuation force is not finite! {}".format(f_r), RuntimeWarning)
        return f_r

    def _propagate(self, r):
        pass


class FluctuationForceRecursive(FluctuationForce):
    """
    As per equations III.22
    """

    def __init__(self, integrator, omega_d, static_coefficients, dt, random_force_autocorrelation):
        super().__init__(integrator, omega_d, static_coefficients, dt, random_force_autocorrelation)
        n = self.statics.n

        is_n_even = n % 2 == 0
        debug("calculating even%s coefficients" % "" if is_n_even else " and odd")
        self.b = zeros([self.statics.hn, n + 1])
        self._calculate_even_coefficients()
        if not is_n_even:
            self._calculate_odd_coefficients()
        debug("%s coefficients = %s" % ("even" if is_n_even else "odd", self.b))

        self.R = zeros([n + 1])
        self.dRdt_nm1 = 0
        self.d2Rdt2_nm1 = 0

    @jit
    def _calculate_even_coefficients(self):
        for m in range(self.statics.n + 1):
            for k in range(self.statics.hn):
                if m == 0 or m == 2 * (k + 1):
                    self.b[k, m] = 1  # III.22 d & e
                elif m == 1 and k == 0:
                    self.b[k, m] = 2 * self.statics.a[1, k]  # III.22 unspecified!
                elif m == 1:
                    self.b[k, m] = 2 * self.statics.a[1, k] * self.b[k - 1, 0] + \
                                   self.b[k - 1, 1]  # III.22 c
                elif m == 2 * k + 1:
                    self.b[k, m] = self.b[k - 1, 2 * k - 1] + \
                                   2 * self.statics.a[1, k] * self.b[k - 1, 2 * k]  # III.22 b
                else:
                    self.b[k, m] = self.b[k - 1, m - 2] + \
                                   2 * self.statics.a[1, k] * self.b[k - 1, m - 1] + self.b[k - 1, m]  # III.22 a

    @jit
    def _calculate_odd_coefficients(self):
        d = ones(self.statics.n + 1)
        for m in range(self.statics.n + 1):
            if m == 0 or m == 2 * self.statics.hn + 3:
                d[m] = 1
            else:
                d[m] = self.b[self.statics.hn - 1, m] + self.b[self.statics.hn - 1, m - 1]

        self.b = zeros([self.statics.hn, self.statics.n + 1])
        self.b[self.statics.hn - 1] = d

    @jit
    def __sum_br(self, offset=0):
        """
        :param offset: offset of 'mode' order indexes in sum
        :type offset: int
        :return: sum of analytical contributions to fluctuation force (from 'modes' of all orders)
        """
        s = 0
        for m in range(self.statics.n + offset):
            s += self.b[self.statics.hn - 1, m] * self.R[m + offset] / pow(self.omega_d, m)
        return pow(self.omega_d, self.statics.n) * s / self.b[self.statics.hn - 1, self.statics.n]

    @jit
    def __forward_euler(self, m, r):
        """
        :param m: 'mode' order
        :type m: int
        :param r: stochastic term in fluctuation force
        :type r: float
        """
        if m == self.statics.n:
            self.R[m] = r - self.__sum_br()
        else:
            self.R[m] += self.R[m + 1] * self.dt

    @jit
    def __symplectic_euler(self, m, r):
        """
        :param m: 'mode' order
        :type m: int
        :param r: stochastic term in fluctuation force
        :type r: float
        :exception: DeprecationWarning
        """
        warn("Symplectic euler algorithm to be updated. Use forward Euler for now.", DeprecationWarning)
        if m == self.statics.n - 1:
            self.R[m] += self.dRdt_nm1 * self.dt + self.d2Rdt2_nm1 * pow(self.dt, 2) / 2
            self.dRdt_nm1 = r - self.__sum_br()
            self.d2Rdt2_nm1 = (r - self.r) / self.dt - self.__sum_br(1)
            self.r = r
        else:
            self.R[m] += self.R[m + 1] * self.dt

    @jit
    def _propagate(self, r):
        """
        :param r: stochastic term in fluctuation force
        :type r: float
        :return: Random force
        :rtype: float
        """
        if self.integrator is IntegrationAlgorithm.FORWARD_EULER:
            for m in reversed(range(self.statics.n)):  # Thanks, Raymond Hettinger!
                self.__forward_euler(m, r / self.b[self.statics.hn - 1, self.statics.n])
        elif self.integrator is IntegrationAlgorithm.SEMI_IMPLICIT_EULER:
            for m in reversed(range(self.statics.n)):
                self.__symplectic_euler(m, r / self.b[self.statics.hn - 1, self.statics.n])
        else:
            raise RuntimeError("Unimplemented IntegrationAlgorithm %s" % self.integrator)
        return self.R[0]


class FluctuationForceDirect(FluctuationForce):
    """
    As per equations III.18
    """

    def __init__(self, integrator, omega_d, static_coefficients, dt, random_force_autocorrelation):
        """
        Zeroth element (self.z[0]) denotes rho (self.q[0] = self.q_dot[0] = 0 for all t, for consistence).
        The last (i.e. self.statics.hn + 1) element (self.z[self.statics.hn + 1]) denotes R(t).
        """
        super().__init__(integrator, omega_d, static_coefficients, dt, random_force_autocorrelation)

        self.z = zeros(self.statics.hn + 2)
        self.q = zeros(self.statics.hn + 2)
        self.q_dot = zeros(self.statics.hn + 2)

    @jit
    def _propagate(self, r):
        """
        :param r: stochastic term in fluctuation force
        :type r: float
        :return: Random force
        :rtype: float
        """
        if self.integrator is IntegrationAlgorithm.FORWARD_EULER:
            self.z[0] = r
            for m in range(1, self.statics.hn + 2):
                self.__forward_euler(m)
        elif self.integrator is IntegrationAlgorithm.VELOCITY_VERLET:
            self.z[0] = r
            for m in range(1, self.statics.hn + 2):
                self.__velocity_verlet(m)
        else:
            raise RuntimeError("Unimplemented IntegrationAlgorithm %s" % self.integrator)
        return self.z[self.statics.hn + 1]

    @jit
    def __forward_euler(self, m):
        self.q[m] += (pow(self.omega_d, 2) * (self.z[m - 1] - self.z[m]) -
                      2 * self.statics.a[1, m] * self.omega_d * self.q[m]) * self.dt
        self.z[m] += self.q[m] * self.dt

    @jit
    def __velocity_verlet(self, m):
        q_dot_prev = self.q_dot[m]
        q_midpoint = self.q[m] - self.q_dot[m] * self.dt / 2
        self.z[m] += q_midpoint * self.dt
        self.q_dot[m] = pow(self.omega_d, 2) * (self.z[m - 1] - self.z[m]) - \
                        2 * self.statics.a[1, m] * self.omega_d * q_midpoint
        self.q[m] += (q_dot_prev + self.q_dot[m]) * self.dt / 2


class DebyeSpectralGenerator:
    """
    :var mass: particle mass, in a.m.u.
    :type mass: int
    :var omega_d:  bath Debye frequency, in wave-numbers
    :type omega_d: float
    :var temperature: bath temperature, in Kelvin
    :type temperature: float
    :type n: int
    :type integrator: IntegrationAlgorithm
    :type dt: float
    :type _static_coefficients: StaticCoefficients
    :type fluctuation: FluctuationForce
    :type dissipation: DissipativeForce
    """
    BathForce = namedtuple('BathForce', ['dissipation', 'fluctuation'])

    # cannot be @jit-ed on Helium...
    def __init__(self, mass, omega_d, temperature, n, is_use_recursive_equations, integrator, sample_rate=5000,
                 dt=None):
        """
        :type self: DebyeSpectralGenerator
        :param mass:
        :param omega_d:
        :param temperature:
        :param n:
        :param integrator:
        :param dt:
        :return:
        """
        info("initializing generator with mass = {}, omega_d = {}, temperature = {}, n = {}, "
             "and dt = {}".format(mass, omega_d, temperature, n, dt))

        assert mass > 0
        self.mass = mass

        assert omega_d > 0
        self.omega_d = omega_d

        assert temperature >= 0
        self.temperature = kelvins_to_wave_numbers(temperature) / omega_d
        info("thermal frequency in Debye frequencies = %f" % self.temperature)

        self.n = n
        self._static_coefficients = StaticCoefficients(self.n)
        if self.n > 0:
            info("n={}, a = {}, A = {}, sigma={}".format(self._static_coefficients.n,
                                                         self._static_coefficients.a.flatten(),
                                                         self._static_coefficients.A.flatten(),
                                                         self._static_coefficients.sigma))

        self.integrator = integrator

        self.sample_rate = sample_rate

        self.dt = dt if dt is not None else self.calculate_dt()
        self.dt *= omega_d
        info("time-step duration in inverse Debye frequencies = %f" % self.dt)
        assert 0 < self.dt < 5e-2

        if self.omega_d != 1:
            warn("Debye frequency should be normalized, but is {}".format(self.omega_d), RuntimeWarning)
        self.dissipation = DissipativeForce(self.mass, self.omega_d, self._static_coefficients, self.dt)

        args = (self.integrator, self.omega_d, self._static_coefficients, self.dt, self.random_force_autocorrelation())
        self.fluctuation = FluctuationForceRecursive(*args) if is_use_recursive_equations \
            else FluctuationForceDirect(*args)

    def calculate_dt(self):
        """
        :return: dt, the time-step duration, in units of inverse Debye frequency
        :rtype: float
        """
        return 2 * math.pi * angular_frequency_to_period(self.omega_d) / self.sample_rate

    def random_force_autocorrelation(self):
        """
        mass * omega_d^3 / k_B * temperature
        :return: the fluctuation force __autocorrelation
        :rtype: float
        """
        if abs(self.temperature) < 1e-5:
            return 0

        return self.mass * pow(self.omega_d, 3) / self.temperature

    # @jit
    def __call__(self, t, v_prev):
        """
        :type self: DebyeSpectralGenerator
        :return: Force exerted by the bath on the particle at the current time-step
        """
        tau = t * self.dt
        # dissipationProcess = Process(target=self.dissipation.__call__, name="dissipation", args=(tau, v_prev))
        # fluctuationProcess = Process(target=self.fluctuation.__call__, name="fluctuation")
        return self.BathForce(self.dissipation(tau, v_prev), self.fluctuation())


def config_fetch_and_update(config, section, option, typ, fallback):
    try:
        if typ == str:
            value = config.get(section, option, fallback=fallback)
        elif typ == bool:
            value = config.getboolean(section, option, fallback=fallback)
        elif typ == int:
            value = config.getint(section, option, fallback=fallback)
        elif typ == float:
            value = config.getfloat(section, option, fallback=fallback)
        else:
            raise TypeError(typ)
    except ValueError as ve:
        warn("ValueError exception in {}:{}={} {} ({})".format(section, option, typ, config.get(section, option),
                                                               fallback), ResourceWarning)
        value = config.get(section, option, fallback=fallback)
        if value == "None":
            return None
        else:
            raise exception(ve)
    if not config.has_section(section):
        config.add_section(section)
    config.set(section, option, str(value))
    return value


def main(config_filename="configuration.ini"):
    setup_logger()

    config = ConfigParser()
    generator = DebyeSpectralGenerator(configure_generator(config, 'resources', config_filename))

    t_f = config_fetch_and_update(config, "simulation", "t_f", float, fallback=1)
    assert t_f > 0
    t_f = picoseconds_to_inverse_wave_numbers(t_f) * generator.omega_d

    info("now calling constructed generator")
    dt = generator.dt
    for tau in range(int(t_f / dt)):
        f = generator(tau, 1)
        info("%.3f, %f %f" % (tau * dt, f.dissipation, f.fluctuation))
    info("generator -- finalized")


def configure_generator(config, resource_path, config_filename):
    config.read(resource_path + '/' + config_filename)

    mass = config_fetch_and_update(config, "particle", "mass", int, fallback=1)
    omega_d = config_fetch_and_update(config, "bath", "omega_d", float, fallback=1)
    temperature = config_fetch_and_update(config, "bath", "temperature", float, fallback=1)
    n = config_fetch_and_update(config, "bath", "n", int, fallback=3)
    is_use_recursive_equations = config_fetch_and_update(config, "simulation", "is_use_recursive_equations", bool,
                                                         fallback=False)
    integrator = config_fetch_and_update(config, "simulation", "integrator", str,
                                         fallback=IntegrationAlgorithm.FORWARD_EULER)
    sample_rate = config_fetch_and_update(config, "simulation", "sample_rate", int, fallback=5000)
    dt = config_fetch_and_update(config, "simulation", "dt", float, fallback=None)

    fp = open(resource_path + '/' + config_filename + '.bak', mode='wt')
    config.write(fp)

    return mass, omega_d, temperature, n, is_use_recursive_equations, integrator, sample_rate, dt


def setup_logger():
    log_file_name = log_file_name_from_path()
    csv_logger_format = "%(relativeCreated)-6d,%(levelname)-8s,%(processName)-12s,%(threadName)-12s,%(name)-4s," \
                        "%(module)-14s,%(funcName)-8s,%(lineno)-4d,%(message)s"
    basicConfig(filename=log_file_name, level=DEBUG, format=csv_logger_format, filemode='wt')
    add_logger_stderr_stream_handler()
    info("ms,level,process,thread,logger,module,function,line,message")
    info("%s activated as statics module" % __file__)
    print(log_file_name)


def add_logger_stderr_stream_handler():
    logger = getLogger()
    stderr_handler = StreamHandler()
    # stderr_handler.setLevel(INFO)
    formatter = Formatter(fmt="%(relativeCreated)-6d %(levelname)-8s %(processName)-12s %(threadName)-12s %("
                              "name)-4s %(module)-14s %(funcName)-8s %(lineno)-4d %(message)s")
    stderr_handler.setFormatter(formatter)
    logger.addHandler(stderr_handler)


def log_file_name_from_path():
    this_file_path = str(__file__)
    try:
        index_last_slash = this_file_path.rindex('/')
    except ValueError:
        index_last_slash = this_file_path.rindex('\\')
    module_name = this_file_path[index_last_slash:-3]
    # module_name = str(__name__)
    return 'out/' + module_name + '_' + current_iso8601_datetime_str() + '.log'


def current_iso8601_datetime_str():
    return str(datetime.now()).replace('-', '').replace(' ', 'T').replace(':', '').split('.')[0]


if __name__ == '__main__':
    parser = ArgumentParser()
    main()
