from distutils.core import setup


def setup_():
    setup(name='debye-spectral-density-generator', version='0.1', packages=[''],
          url='https://bitbucket.org/heat-conduction-mol-junctions/debye-spectral-density-generator',
          author='Inon Sharony',
          author_email='InonShar@TAU.ac.IL', description='Implementation of DOI: 10.1063/1.436895',
          requires=['numpy', 'numba', 'matplotlib', 'sphinx_rtd_theme', 'nose'])


if __name__ == '__main__':
    setup_()
